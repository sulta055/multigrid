! Implementation of the Full multi-grid (FMG) v-cycle iterative method for the 2D poissons equation
! Errors are computed relative to an exact solution
! Reference: "A Multigrid Tutorial", Briggs, 2000

PROGRAM poisson_2d_fullmultigrid_vcycle

USE MPI
IMPLICIT NONE


TYPE gridtype

    INTEGER :: nn
    REAL, ALLOCATABLE :: u(:,:), fv(:,:), f(:,:), r(:,:)

END TYPE gridtype

INTEGER, PARAMETER :: N = 256     ! size opf main grid
INTEGER, PARAMETER :: levels = 6  ! number of levels
INTEGER, PARAMETER :: vcycles = 1 ! number of v-clcles per level
LOGICAL, PARAMETER :: bypass2grid = .TRUE.
INTEGER, PARAMETER :: smoother_type = 1 ! 1: SOR, 2: RED-BLACK SOR
INTEGER :: i, j
REAL :: t1, t2, t3
REAL, ALLOCATABLE :: uexact(:,:), u2(:,:)

! define array of grid structures
TYPE(gridtype) :: grid(0:levels) 

! initialze grid
CALL init_grid()

! solve poissons equation
t1 = MPI_WTIME()

CALL vcycle(0)
u2 = grid(0)%u

t2 = MPI_WTIME()

grid(0)%u = 0.0
CALL solve_fmg()

t3 = MPI_WTIME()

! save to file
CALL fileoutput()

CALL destroy_grid()


PRINT*,''
PRINT*,'MG V-cycle solve time (sec) = ',t2-t1
PRINT*,'FMG V-cycle solve time (sec) = ',t3-t2
PRINT*,''
PRINT*,'Done.'

CONTAINS



SUBROUTINE init_grid()

    INTEGER :: i, j, k, mem
    REAL :: x, y, dx
    
    mem = 0  

    DO k = 0, levels

        j = N/(2**k) 
        grid(k)%nn = j
        ALLOCATE(grid(k)%u(0:j,0:j))
        ALLOCATE(grid(k)%fv(0:j,0:j))
        ALLOCATE(grid(k)%f(0:j,0:j))
        ALLOCATE(grid(k)%r(0:j,0:j))
        grid(k)%u = 0.0
        grid(k)%fv = 0.0
        grid(k)%f = 0.0
        grid(k)%r = 0.0
        
        PRINT*,''
        WRITE(*,FMT='("Grid level = ",i4,", N = ",i4)')k,j
        PRINT*,''
        
        mem = mem +  4 * 4 * (j+1)**2

    END DO

    ALLOCATE(uexact(0:N,0:N),u2(0:N,0:N))
    uexact = 0.0
    u2 = 0.0

    dx = 1.0/SNGL(N)
  
    ! fill exact solution array 
    DO j = 0, N
        y = j*dx
        DO i = 0, N
            x = i*dx
            uexact(i,j) = (x**2-x**4)*(y**4-y**2)        
        END DO      
    END DO
 
    ! fill source term array for each grid level
    DO k = 0, levels
        dx = 1.0/SNGL(grid(k)%nn)
        DO j = 0, grid(k)%nn
            y = j*dx
            DO i = 0, grid(k)%nn
                x = i*dx
                grid(k)%fv(i,j) = - 2.0 * ((1.0-6.0*x**2)*(y**2)*(1.0-y**2)+((1.0-6.0*y**2)*(x**2)*(1.0-x**2)))
            END DO      
        END DO
    END DO


    PRINT*,''
    PRINT*,'Total memory for grid storage (Mb) = ',mem*1e-6
    PRINT*,''
   

END SUBROUTINE init_grid


SUBROUTINE destroy_grid()

    DEALLOCATE(uexact,u2)

END SUBROUTINE destroy_grid



SUBROUTINE solve_fmg()

    INTEGER :: i, j, k, lv
    INTEGER, PARAMETER :: coarse_iter = 300
    REAL :: l2err, l2r

    ! compute the solution at the coarsest level
    grid(levels)%u = 0.0 ! initial guess
    IF(smoother_type .EQ. 1) CALL relaxation_sor(grid(levels)%nn,coarse_iter,grid(levels)%u,grid(levels)%fv,.TRUE.) 
    IF(smoother_type .EQ. 2) CALL relaxation_sor_redblack(grid(levels)%nn,coarse_iter,grid(levels)%u,grid(levels)%fv,.TRUE.) 
 
    ! FMG v-cycle loop
    DO lv = levels-1, 0, -1
        
        PRINT*,'FMG Level #',lv
        
        DO k = 1, vcycles
    
            IF(k .EQ. 1) THEN
                
                ! interpolate the solution from previous v-cycle onto the next higher grid level (to be used as the initial guess for the v-cycle at that level)
                DO j = 2, grid(lv)%nn, 2
                    DO i = 2, grid(lv)%nn, 2

                        ! for even numbered points, no interpolation required
                        grid(lv)%u(i,j) = grid(lv+1)%u(i/2,j/2) 

                        ! for odd numbered points, we use linear interpolation
                        grid(lv)%u(i-1,j)   = 0.5 *  (grid(lv+1)%u(i/2 - 1,j/2) + grid(lv+1)%u(i/2,j/2)) 
                        grid(lv)%u(i-1,j-1) = 0.25 * (grid(lv+1)%u(i/2 - 1,j/2) + grid(lv+1)%u(i/2,j/2) + grid(lv+1)%u(i/2,j/2 - 1) + grid(lv+1)%u(i/2-1,j/2-1))
                        grid(lv)%u(i,j-1)   = 0.5 *  (grid(lv+1)%u(i/2,j/2 - 1) + grid(lv+1)%u(i/2,j/2))          
                            
                    END DO
                END DO

            END IF
            
            ! do a v-cycle at this level
            CALL vcycle(lv)
 
        END DO
    END DO
    
    ! compute error l2 norm
    l2err = SQRT(SUM((grid(0)%u-uexact)**2))

    PRINT*,''
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,''
    
    
END SUBROUTINE solve_fmg



! this subroutine computes a v-cycle at level 'lv' (lv = 0 is just a single V-cycle performed on the finest/main grid)
SUBROUTINE vcycle(lv)

    INTEGER, INTENT(IN) :: lv 
    INTEGER :: i, j, k, nu1, nu2


    nu1 = 3  ! number of smoothing sweeps
    nu2 = 3  ! number of post-smoothing sweeps

    
    !#######################
    ! V -cycle forward leg
    !######################

    PRINT*,'Computing V-cycle forward leg...'
    
    grid(lv)%f = grid(lv)%fv 
    
    DO k = lv+1, levels+1
    
        !********************************
        ! Step 1: Relaxation on fine grid
        !********************************
        
        ! relaxation on grid nu1 times
        IF(smoother_type .EQ. 1) CALL relaxation_sor(grid(k-1)%nn,nu1,grid(k-1)%u,grid(k-1)%f,.TRUE.) ! using SOR for smoothing 
        IF(smoother_type .EQ. 2) CALL relaxation_sor_redblack(grid(k-1)%nn,nu1,grid(k-1)%u,grid(k-1)%f,.TRUE.) ! using SOR for smoothing 

        ! If this is the coarsest grid level, then stop here. 
        IF(k .EQ. levels+1) EXIT    
        
        
        !*****************************************************************
        ! Step 2: Restriction, i.e. interpolation from fine to coarse grid
        !****************************************************************
        
        ! compute the fine-grid residual
        CALL compute_residual(grid(k-1)%nn,grid(k-1)%u,grid(k-1)%r,grid(k-1)%f)
        
        ! transfer fine-grid residuals onto coarse grid via interpolation
        DO j = 0, grid(k)%nn
            DO i = 0, grid(k)%nn
                    
                ! linear interpolation (i.e. nine-point weighted average)
                grid(k)%f(i,j) = (1.0/16.0) * (4.0 * grid(k-1)%r(2*i,2*j) + 2.0 * ( grid(k-1)%r(2*i-1,2*j) + &
                                         grid(k-1)%r(2*i+1,2*j)+grid(k-1)%r(2*i,2*j+1)+grid(k-1)%r(2*i,2*j-1) ) + &
                                       ( grid(k-1)%r(2*i-1,2*j-1)+grid(k-1)%r(2*i-1,2*j+1)+grid(k-1)%r(2*i+1,2*j-1)+grid(k-1)%r(2*i+1,2*j+1) ))  
                                                                 
            END DO
        END DO
        
        ! set initial guess for coarse grid relaxation
        grid(k)%u = 0.0
             
    END DO
   

    !#######################
    ! V-cycle return leg
    !#######################

    PRINT*,'Computing V-cycle return leg...'
    
    DO k = levels-1, lv, -1
   
        !*********************************************************************************************************************
        ! Step 3: Prolongation (i.e. interpolation from coarse back to fine grid) and Correction (to obtain new initial guess) 
        !*********************************************************************************************************************

        ! linearly interpolated coarse-grid error/correction onto fine grid and add to the solution to get new improved initial guess 
        DO j = 2, grid(k)%nn, 2
            DO i = 2, grid(k)%nn, 2

                ! for even numbered points, no interpolation required
                grid(k)%u(i,j) = grid(k)%u(i,j) + grid(k+1)%u(i/2,j/2) 

                ! for odd numbered points, we use linear interpolation
                grid(k)%u(i-1,j)   = grid(k)%u(i-1,j) + 0.5 *    (grid(k+1)%u(i/2 - 1,j/2) + grid(k+1)%u(i/2,j/2)) 
                grid(k)%u(i-1,j-1) = grid(k)%u(i-1,j-1) + 0.25 * (grid(k+1)%u(i/2 - 1,j/2) + grid(k+1)%u(i/2,j/2) + grid(k+1)%u(i/2,j/2 - 1) + grid(k+1)%u(i/2-1,j/2-1))
                grid(k)%u(i,j-1)   = grid(k)%u(i,j-1) + 0.5 *    (grid(k+1)%u(i/2,j/2 - 1) + grid(k+1)%u(i/2,j/2))          
                    
            END DO
        END DO

        !********************************************************************************************************************************************
        ! Step 4: Post Relaxation/ Smoothing Sweeps (i.e. we do some more iterations on the fine grid to refine the solution using new initial guess)
        !********************************************************************************************************************************************
        IF(smoother_type .EQ. 1) CALL relaxation_sor(grid(k)%nn,nu2,grid(k)%u,grid(k)%f,.TRUE.) ! using SOR for smoothing  
        IF(smoother_type .EQ. 2) CALL relaxation_sor_redblack(grid(k)%nn,nu2,grid(k)%u,grid(k)%f,.TRUE.) ! using SOR for smoothing  
     
    END DO        
    

END SUBROUTINE vcycle



! Successive Over-relaxation (SOR) iterations
SUBROUTINE relaxation_sor(nn,iters,u,f,relax)

    INTEGER, INTENT(IN) :: nn, iters
    REAL, INTENT(INOUT) :: u(0:nn,0:nn), f(0:nn,0:nn)
    LOGICAL, INTENT(IN) :: relax
    INTEGER :: i, j, k
    REAL :: x, y, dx, dx2, w, wopt, l2r, dl2r
    REAL, ALLOCATABLE :: rc(:,:)
    REAL, PARAMETER :: rtol = 1.d-5    
    REAL, PARAMETER :: PI  = 4.d0*ATAN(1.d0)

    k = 0
    dl2r = 1.0
    l2r = rtol + dl2r
    dx = 1.0/SNGL(nn)
    dx2 = dx**2
    ! set relaxation parameter
    wopt = 2.0/(1.0+SIN(PI*dx)) ! SOR optimal value
    w = wopt             ! 1<w<=2
    
    ! for relaxation, we do a fixed number of iterations
    IF(relax) THEN
        
        ! relaxation/iterations loop
        DO k = 1, iters

            ! compute next iteration 
            DO j = 1, nn-1
                DO i = 1, nn-1
                    
                    u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

        END DO

    ! otherwise, we do a full iterative solve
    ELSE 
    
        ALLOCATE(rc(0:nn,0:nn))
    
        ! relaxation/iterations loop
        DO WHILE(k .LT. iters .AND. dl2r .GT. rtol)

            ! compute next iteration 
            DO j = 1, nn-1
                DO i = 1, nn-1
                    
                    u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

            ! compute residual l2-norm
            CALL compute_residual(nn,u,rc,f)
            
            dl2r = l2r
            l2r = SQRT(SUM(rc**2))           
            dl2r = ABS(dl2r-l2r) 
            
            ! increment iteration count
            k = k + 1

        END DO
        
        PRINT*,'# of iterations = ',k
        PRINT*,'d12r = ',dl2r
        
        DEALLOCATE(rc)
    
    END IF


END SUBROUTINE relaxation_sor


! Successive Over-relaxation (SOR) iterations
SUBROUTINE relaxation_sor_redblack(nn,iters,u,f,relax)

    INTEGER, INTENT(IN) :: nn, iters
    REAL, INTENT(INOUT) :: u(0:nn,0:nn), f(0:nn,0:nn)
    LOGICAL, INTENT(IN) :: relax
    INTEGER :: i, j, k
    REAL :: x, y, dx, dx2, w, wopt, l2r, dl2r
    REAL, ALLOCATABLE :: rc(:,:)
    REAL, PARAMETER :: rtol = 1.d-5    
    REAL, PARAMETER :: PI  = 4.d0*ATAN(1.d0)
    REAL, ALLOCATABLE :: buffer(:,:)
    
    
    ALLOCATE(buffer(0:nn,0:nn))
    k = 0
    dl2r = 1.0
    l2r = rtol + dl2r
    dx = 1.0/SNGL(nn)
    dx2 = dx**2
    ! set relaxation parameter
    wopt = 2.0/(1.0+SIN(PI*dx)) ! SOR optimal value
    w = wopt             ! 1<w<=2
    
    ! for relaxation, we do a fixed number of iterations
    IF(relax) THEN
        
        ! relaxation/iterations loop
        DO k = 1, iters

            ! compute next iteration 
            
            ! red pass
            buffer = u
            DO j = 1, nn-1
                DO i = 1+MOD(j+1,2), nn-1, 2
                    
                    u(i,j) = (1.0-w) * buffer(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

            ! black pass
            buffer = u
            DO j = 1, nn-1
                DO i = 1+MOD(j,2), nn-1, 2
                    
                    u(i,j) = (1.0-w) * buffer(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO


        END DO

    ! otherwise, we do a full iterative solve
    ELSE 
    
        ALLOCATE(rc(0:nn,0:nn))
    
        ! relaxation/iterations loop
        DO WHILE(k .LT. iters .AND. dl2r .GT. rtol)

            ! compute next iteration 

            ! red pass
            buffer = u
            DO j = 1, nn-1
                DO i = 1+MOD(j,2), nn-1-MOD(j,2), 2
                    
                    u(i,j) = (1.0-w) * buffer(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

            ! black pass
            buffer = u
            DO j = 1, nn-1
                DO i = 1+MOD(j+1,2), nn-1-MOD(j+1,2), 2
                    
                    u(i,j) = (1.0-w) * buffer(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

            ! compute residual l2-norm
            CALL compute_residual(nn,u,rc,f)
            
            dl2r = l2r
            l2r = SQRT(SUM(rc**2))           
            dl2r = ABS(dl2r-l2r) 
            
            ! increment iteration count
            k = k + 1

        END DO
        
        PRINT*,'# of iterations = ',k
        PRINT*,'d12r = ',dl2r
        
        DEALLOCATE(rc)
    
    END IF

    DEALLOCATE(buffer)

END SUBROUTINE relaxation_sor_redblack


SUBROUTINE compute_residual(nn,u,r,f)

    INTEGER, INTENT(IN) :: nn
    REAL, INTENT(IN) :: u(0:nn,0:nn), f(0:nn,0:nn)
    REAL, INTENT(OUT) :: r(0:nn,0:nn)
    INTEGER :: i, j
    REAL x, y, dx, idx2

    idx2 = SNGL(nn**2)
    
    ! clear residual array
    r = 0.d0

    ! compute residual at every grid point
    DO j = 1, nn-1
        DO i = 1, nn-1
            r(i,j) = f(i,j) - idx2*(u(i+1,j) + u(i-1,j) - 4.0 * u(i,j) + u(i,j+1) + u(i,j-1)) 
        END DO
    END DO


END SUBROUTINE compute_residual
    
    
    
SUBROUTINE fileoutput()

    INTEGER :: i, j
    REAL :: x, y, dx

    dx = 1.0/SNGL(N)

    OPEN(UNIT=10, FILE='Output/output.dat', FORM = 'UNFORMATTED', ACCESS='STREAM')

    DO j = 0, N
        y = j*dx
        DO i = 0, N
            x = i*dx
            WRITE(10) x,y,uexact(i,j),grid(0)%u(i,j),u2(i,j)
        END DO
    END DO


    CLOSE(UNIT=10)
    
END SUBROUTINE fileoutput 



END PROGRAM poisson_2d_fullmultigrid_vcycle
