! Implementation of the basic multi-grid v-cycle iterative method for the 2D poissons equation
! Errors are computed relative to an exact solution
! Reference: "A Multigrid Tutorial", Briggs, 2000

PROGRAM poisson_2d_multigrid_vcycle

USE MPI
IMPLICIT NONE


TYPE gridtype

    INTEGER :: nn
    REAL, ALLOCATABLE :: u(:,:), f(:,:), r(:,:)

END TYPE gridtype

INTEGER, PARAMETER :: N = 256
INTEGER, PARAMETER :: levels = 6
REAL, PARAMETER :: tol = 1.e-2
REAL*8,  PARAMETER :: PI  = 4.d0*ATAN(1.d0)
LOGICAL, PARAMETER :: bypass2grid = .TRUE.
!INTEGER, PARAMETER :: smoother_type = 3 ! 1: JACOBI, 2: GS, 3: SOR, 4: WEIGHTED JACOBI
INTEGER :: i, j
REAL :: t1, t2, t3
REAL, ALLOCATABLE :: uexact(:,:), u2(:,:)

! define array of grid structures
TYPE(gridtype) :: grid(0:levels) 

! initialze grid
CALL init_grid()

! solve poissons equation
t1 = MPI_WTIME()
CALL solve_mg_vcycle()
t2 = MPI_WTIME()
CALL relaxation_sor(N,300,u2,grid(0)%f,.FALSE.) 
t3 = MPI_WTIME()

! save to file
CALL fileoutput()

CALL destroy_grid()


PRINT*,''
PRINT*,'One grid solve time (sec) = ',t3-t2
PRINT*,'MG V-cycle solve time (sec) = ',t2-t1
PRINT*,''
PRINT*,'Done.'

CONTAINS



SUBROUTINE init_grid()

    INTEGER :: i, j, mem
    REAL :: x, y, dx
    
    mem = 0  

    DO i = 0, levels

        j = N/(2**i) 
        grid(i)%nn = j
        ALLOCATE(grid(i)%u(0:j,0:j))
        ALLOCATE(grid(i)%f(0:j,0:j))
        ALLOCATE(grid(i)%r(0:j,0:j))
        grid(i)%u = 0.0
        grid(i)%f = 0.0
        grid(i)%r = 0.0
        
        PRINT*,''
        WRITE(*,FMT='("Grid level = ",i4,", N = ",i4)')i,j
        PRINT*,''
        
        mem = mem +  4 * 3 * (j+1)**2

    END DO

    ALLOCATE(uexact(0:N,0:N),u2(0:N,0:N))
    uexact = 0.0
    u2 = 0.0

    dx = 1.0/SNGL(N)
  
    ! fill exact solution array and source term array for main (i.e. finest) grid
    DO j = 0, N
        y = j*dx
        DO i = 0, N
            x = i*dx
            uexact(i,j) = (x**2-x**4)*(y**4-y**2)        
            grid(0)%f(i,j) = - 2.0 * ((1.0-6.0*x**2)*(y**2)*(1.0-y**2)+((1.0-6.0*y**2)*(x**2)*(1.0-x**2)))
        END DO
                
    END DO

    PRINT*,''
    PRINT*,'Total memory for grid storage (Mb) = ',mem*1e-6
    PRINT*,''
   

END SUBROUTINE init_grid


SUBROUTINE destroy_grid()

    DEALLOCATE(uexact,u2)

END SUBROUTINE destroy_grid


SUBROUTINE solve_mg_vcycle()

    INTEGER :: i, j, k, nu1, nu2
    REAL :: l2err, l2r


    nu1 = 3  ! number of smoothing sweeps
    nu2 = 3  ! number of post-smoothing sweeps

    ! set initial guess for the main grid relaxation
    grid(0)%u = 0.0
    
    PRINT*,'Doing V-cycle forward cycle...'
    
    ! V -cycle forward leg
    DO k = 1, levels+1
    
        !********************************
        ! Step 1: Relaxation on fine grid
        !********************************
        
        ! relaxation on grid nu1 times
        CALL relaxation_sor(grid(k-1)%nn,nu1,grid(k-1)%u,grid(k-1)%f,.TRUE.) ! using SOR for smoothing 

        ! If this is the coarsest grid level, then stop here. 
        IF(k .EQ. levels+1) EXIT
                
        !*****************************************************************
        ! Step 2: Restriction, i.e. interpolation from fine to coarse grid
        !****************************************************************
        
        ! compute the fine-grid residual
        CALL compute_residual(grid(k-1)%nn,grid(k-1)%u,grid(k-1)%r,grid(k-1)%f)
        
        ! transfer fine-grid residuals onto coarse grid via interpolation
        DO j = 0, grid(k)%nn
            DO i = 0, grid(k)%nn
                    

                ! linear interpolation (i.e. nine-point weighted average)
                grid(k)%f(i,j) = (1.0/16.0) * (4.0 * grid(k-1)%r(2*i,2*j) + 2.0 * ( grid(k-1)%r(2*i-1,2*j) + &
                                         grid(k-1)%r(2*i+1,2*j)+grid(k-1)%r(2*i,2*j+1)+grid(k-1)%r(2*i,2*j-1) ) + &
                                       ( grid(k-1)%r(2*i-1,2*j-1)+grid(k-1)%r(2*i-1,2*j+1)+grid(k-1)%r(2*i+1,2*j-1)+grid(k-1)%r(2*i+1,2*j+1) ))  
                                                                 
            END DO
        END DO
        
        ! set initial guess for coarse grid relaxation
        grid(k)%u = 0.0
             
    END DO
   

    PRINT*,'Doing V-cycle return cycle...'

    ! V-cycle return leg
    DO k = levels-1, 0, -1
   
        !*********************************************************************************************************************
        ! Step 3: Prolongation (i.e. interpolation from coarse back to fine grid) and Correction (to obtain new initial guess) 
        !*********************************************************************************************************************

        ! linearly interpolated coarse-grid error/correction onto fine grid and add to the solution to get new improved initial guess 
        DO j = 2, grid(k)%nn, 2
            DO i = 2, grid(k)%nn, 2

                ! for even numbered points, no interpolation required
                grid(k)%u(i,j) = grid(k)%u(i,j) + grid(k+1)%u(i/2,j/2) 

                ! for odd numbered points, we use linear interpolation
                grid(k)%u(i-1,j)   = grid(k)%u(i-1,j) + 0.5 * (grid(k+1)%u(i/2 - 1,j/2) + grid(k+1)%u(i/2,j/2)) 
                grid(k)%u(i-1,j-1) = grid(k)%u(i-1,j-1) + 0.25 * (grid(k+1)%u(i/2 - 1,j/2) + grid(k+1)%u(i/2,j/2) + grid(k+1)%u(i/2,j/2 - 1) + grid(k+1)%u(i/2-1,j/2-1))
                grid(k)%u(i,j-1)   = grid(k)%u(i,j-1) + 0.5 * (grid(k+1)%u(i/2,j/2 - 1) + grid(k+1)%u(i/2,j/2))          
                    
            END DO
        END DO

        !********************************************************************************************************************************************
        ! Step 4: Post Relaxation/ Smoothing Sweeps (i.e. we do some more iterations on the fine grid to refine the solution using new initial guess)
        !********************************************************************************************************************************************
        CALL relaxation_sor(grid(k)%nn,nu2,grid(k)%u,grid(k)%f,.TRUE.) ! using SOR for smoothing  
     
    END DO        
    
    ! compute error l2 norm
    l2err = SQRT(SUM((grid(0)%u-uexact)**2))

    PRINT*,''
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,''


END SUBROUTINE solve_mg_vcycle



! Successive Over-relaxation (SOR) iterations
SUBROUTINE relaxation_sor(nn,iters,u,f,relax)

    INTEGER, INTENT(IN) :: nn, iters
    REAL, INTENT(INOUT) :: u(0:nn,0:nn), f(0:nn,0:nn)
    LOGICAL, INTENT(IN) :: relax
    INTEGER :: i, j, k
    REAL :: x, y, dx, dx2, w, wopt, l2r, dl2r
    REAL, ALLOCATABLE :: rc(:,:)
    REAL, PARAMETER :: rtol = 1.d-5    
    
    k = 0
    dl2r = 1.0
    l2r = rtol + dl2r
    dx = 1.0/SNGL(nn)
    dx2 = dx**2
    ! set relaxation parameter
    wopt = 2.0/(1.0+SIN(PI*dx)) ! SOR optimal value
    w = wopt             ! 1<w<=2
    
    ! for relaxation, we do a fixed number of iterations
    IF(relax) THEN
        
        ! relaxation/iterations loop
        DO k = 1, iters

            ! compute next iteration 
            DO j = 1, nn-1
                DO i = 1, nn-1
                    
                    u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

        END DO

    ! otherwise, we do a full iterative solve
    ELSE 
    
        ALLOCATE(rc(0:nn,0:nn))
    
        ! relaxation/iterations loop
        DO WHILE(k .LT. iters .AND. dl2r .GT. rtol)

            ! compute next iteration 
            DO j = 1, nn-1
                DO i = 1, nn-1
                    
                    u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2 * f(i,j)) 
            
                END DO
            END DO

            ! compute residual l2-norm
            CALL compute_residual(nn,u,rc,f)
            
            dl2r = l2r
            l2r = SQRT(SUM(rc**2))           
            dl2r = ABS(dl2r-l2r) 
            
            ! increment iteration count
            k = k + 1

        END DO
        
        PRINT*,'# of iterations = ',k
        PRINT*,'d12r = ',dl2r
        
        DEALLOCATE(rc)
    
    END IF


END SUBROUTINE relaxation_sor


SUBROUTINE compute_residual(nn,u,r,f)

    INTEGER, INTENT(IN) :: nn
    REAL, INTENT(IN) :: u(0:nn,0:nn), f(0:nn,0:nn)
    REAL, INTENT(OUT) :: r(0:nn,0:nn)
    INTEGER :: i, j
    REAL x, y, dx, idx2

    idx2 = SNGL(nn**2)
    
    ! clear residual array
    r = 0.d0

    ! compute residual at every grid point
    DO j = 1, nn-1
        DO i = 1, nn-1
            r(i,j) = f(i,j) - idx2*(u(i+1,j) + u(i-1,j) - 4.0 * u(i,j) + u(i,j+1) + u(i,j-1)) 
        END DO
    END DO


END SUBROUTINE compute_residual
    
    
    
SUBROUTINE fileoutput()

    INTEGER :: i, j
    REAL :: x, y, dx

    dx = 1.0/SNGL(N)

    OPEN(UNIT=10, FILE='Output/output.dat', FORM = 'UNFORMATTED', ACCESS='STREAM')

    DO j = 0, N
        y = j*dx
        DO i = 0, N
            x = i*dx
            WRITE(10) x,y,uexact(i,j),grid(0)%u(i,j),u2(i,j)
        END DO
    END DO


    CLOSE(UNIT=10)
    
END SUBROUTINE fileoutput 



END PROGRAM poisson_2d_multigrid_vcycle
