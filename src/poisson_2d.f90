! Implementation of basic iterative methods for the 2D poissons equation: Jacobi, Gauss-Seidel, SOR and Weighted Jacobi
! Errors are computed relative to an exact solution

PROGRAM poisson_2d

USE MPI
IMPLICIT NONE

INTEGER, PARAMETER :: N = 256
INTEGER, PARAMETER :: maxiter = 1000
REAL, PARAMETER :: tol = 1.e-2
REAL*8,  PARAMETER :: PI  = 4.d0*ATAN(1.d0)
INTEGER, PARAMETER :: solver_type = 3 ! 1: JACOBI, 2: GS, 3: SOR, 4: WEIGHTED JACOBI
INTEGER :: i,j
REAL :: x, y, dx, t1, t2
REAL, ALLOCATABLE :: u(:,:), uexact(:,:), r(:,:)


ALLOCATE(u(0:N,0:N), uexact(0:N,0:N) , r(0:N,0:N))
u = 0.0
uexact = 0.0

! grid resolution
dx = 1.d0/SNGL(N)


! fill exact solution array
DO j = 0, N
    y = j*dx
    DO i = 0, N
        x = i*dx
        uexact(i,j) = (x**2-x**4)*(y**4-y**2)        
    END DO
END DO

! set initial guess
u = 0.d0

! solve poissons equation
t1 = MPI_WTIME()
IF(solver_type .EQ. 1) CALL solve_jacobi()
IF(solver_type .EQ. 2) CALL solve_gs()
IF(solver_type .EQ. 3) CALL solve_sor()
IF(solver_type .EQ. 4) CALL solve_wjacobi()
IF(solver_type .EQ. 5) CALL solve_gs_redblack()
IF(solver_type .EQ. 6) CALL solve_sor_redblack()

t2 = MPI_WTIME()


DEALLOCATE(u,uexact,r)


PRINT*,''
PRINT*,'Solve time (sec) = ',t2-t1
PRINT*,''
PRINT*,'Done.'

CONTAINS

! Jacobi iterations
SUBROUTINE solve_jacobi()

    INTEGER :: i, j, k
    REAL :: l2err, x, y, dx2, f
    REAL, ALLOCATABLE :: buffer(:,:)
    
    
    ALLOCATE(buffer(0:N,0:N))
    k = 0
    l2err = tol + 1.d0
    dx2 = dx**2
      
    ! relaxation/iterations loop
    DO WHILE(k .LT. maxiter .AND. l2err .GT. tol)

        !PRINT*,'Iterations # ',k+1

        ! copy intial state into buffer
        buffer = u

        ! compute next iteration 
        DO j = 1, N-1
            y = j * dx
            DO i = 1, N-1
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f ) 
        
            END DO
        END DO

        ! compute l2 error
        l2err = SQRT(SUM((u-uexact)**2))
        
        ! increment iteration count
        k = k + 1

    END DO

    PRINT*,''
    PRINT*,'Iterations loop completed'
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,'# of iterations = ',k
    IF(k .GE. maxiter) PRINT*,'Iterations failed to converge.'
    PRINT*,''

    DEALLOCATE(buffer)

END SUBROUTINE solve_jacobi

! Gauss Seidel iterations
SUBROUTINE solve_gs()

    INTEGER :: i, j, k
    REAL :: l2err, x, y, dx2, f
    
    k = 0
    l2err = tol + 1.d0
    dx2 = dx**2
    
    ! relaxation/iterations loop
    DO WHILE(k .LT. maxiter .AND. l2err .GT. tol)

        !PRINT*,'Iterations # ',k+1


        ! compute next iteration 
        DO j = 1, N-1
            y = j * dx
            DO i = 1, N-1
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2*f ) 
        
            END DO
        END DO

        ! compute l2 error
        l2err = SQRT(SUM((u-uexact)**2))
        
        ! increment iteration count
        k = k + 1

    END DO

    PRINT*,''
    PRINT*,'Iterations loop completed'
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,'# of iterations = ',k
    IF(k .GE. maxiter) PRINT*,'Iterations failed to converge.'
    PRINT*,''

END SUBROUTINE solve_gs


SUBROUTINE solve_gs_redblack()

    INTEGER :: i, j, k
    REAL :: l2err, x, y, dx2, f
    REAL, ALLOCATABLE :: buffer(:,:)
    
    
    ALLOCATE(buffer(0:N,0:N))
    k = 0
    l2err = tol + 1.d0
    dx2 = dx**2
    
    ! relaxation/iterations loop
    DO WHILE(k .LT. maxiter .AND. l2err .GT. tol)

        !PRINT*,'Iterations # ',k+1
          
        ! red pass
        buffer = u        
        DO j = 1, N-1
            y = j * dx
            DO i = 1+MOD(j,2), N-1-MOD(j,2), 2
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f ) 
        
            END DO
        END DO


        ! black pass
        buffer = u        
        DO j = 1, N-1
            y = j * dx
            DO i = 1+MOD(j+1,2), N-1-MOD(j+1,2), 2
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f ) 
        
            END DO
        END DO

        ! compute l2 error
        l2err = SQRT(SUM((u-uexact)**2))
        
        ! increment iteration count
        k = k + 1

    END DO

    PRINT*,''
    PRINT*,'Iterations loop completed'
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,'# of iterations = ',k
    IF(k .GE. maxiter) PRINT*,'Iterations failed to converge.'
    PRINT*,''

    DEALLOCATE(buffer)

END SUBROUTINE solve_gs_redblack

! Successive Over-relaxation (SOR) iterations
SUBROUTINE solve_sor()

    INTEGER :: i, j, k
    REAL :: l2err, l2r,  x, y, dx2, f, w, wopt
    
    k = 0
    l2err = tol + 1.d0
    dx2 = dx**2
   
    ! set relaxation parameter
    wopt = 2.0/(1.0+SIN(PI*dx)) ! SOR optimal value
    w = wopt ! 1<w<=2
    
    
    ! relaxation/iterations loop
    DO WHILE(k .LT. maxiter .AND. l2err .GT. tol)

        !PRINT*,'Iterations # ',k+1


        ! compute next iteration 
        DO j = 1, N-1
            y = j * dx
            DO i = 1, N-1
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = (1.0-w)*u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2*f ) 
        
            END DO
        END DO

        ! compute l2 error
        l2err = SQRT(SUM((u-uexact)**2))
        
        ! compute residual
        CALL compute_residual(u,r)
        l2r = SQRT(SUM(r**2))

        ! increment iteration count
        k = k + 1

    END DO

    PRINT*,''
    PRINT*,'Iterations loop completed'
    PRINT*,'l2 error norm = ',l2err 
    PRINT*,'l2r = ',l2r      
    PRINT*,'# of iterations = ',k
    IF(k .GE. maxiter) PRINT*,'Iterations failed to converge.'
    PRINT*,''

END SUBROUTINE solve_sor


SUBROUTINE solve_sor_redblack()

    INTEGER :: i, j, k
    REAL :: l2err, l2r,  x, y, dx2, f, w, wopt
    REAL, ALLOCATABLE :: buffer(:,:)
    
    
    ALLOCATE(buffer(0:N,0:N))
    k = 0
    l2err = tol + 1.d0
    dx2 = dx**2
   
    ! set relaxation parameter
    wopt = 2.0/(1.0+SIN(PI*dx)) ! SOR optimal value
    w = wopt ! 1<w<=2
    
    
    ! relaxation/iterations loop
    DO WHILE(k .LT. maxiter .AND. l2err .GT. tol)

        !PRINT*,'Iterations # ',k+1

   
        ! compute next iteration 

        ! red pass
        buffer = u        
        DO j = 1, N-1
            y = j * dx
            DO i = 1+MOD(j,2), N-1-MOD(j,2), 2
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = (1.0-w)*buffer(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f )  
        
            END DO
        END DO

        ! black pass
        buffer = u        
        DO j = 1, N-1
            y = j * dx
            DO i = 1+MOD(j+1,2), N-1-MOD(j+1,2), 2
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = (1.0-w)*buffer(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f )  
        
            END DO
        END DO
        

        ! compute l2 error
        l2err = SQRT(SUM((u-uexact)**2))
        
        ! compute residual
        CALL compute_residual(u,r)
        l2r = SQRT(SUM(r**2))

        ! increment iteration count
        k = k + 1

    END DO

    PRINT*,''
    PRINT*,'Iterations loop completed'
    PRINT*,'l2 error norm = ',l2err 
    PRINT*,'l2r = ',l2r      
    PRINT*,'# of iterations = ',k
    IF(k .GE. maxiter) PRINT*,'Iterations failed to converge.'
    PRINT*,''

    DEALLOCATE(buffer)

END SUBROUTINE solve_sor_redblack


! Weighted Jacobi iterations
SUBROUTINE solve_wjacobi()

    INTEGER :: i, j, k
    REAL :: l2err, l2r, x, y, dx2, f, w, wopt
    REAL, ALLOCATABLE :: buffer(:,:)
    
    
    ALLOCATE(buffer(0:N,0:N))
    k = 0
    l2err = tol + 1.d0
    dx2 = dx**2
    
    ! set relaxation parameter
    wopt = 0.8 !weighted jacobi optimal value
    w = wopt  ! w<=1
    
    ! relaxation/iterations loop
    DO WHILE(k .LT. maxiter .AND. l2err .GT. tol)

        !PRINT*,'Iterations # ',k+1

        ! copy intial state into buffer
        buffer = u

        ! compute next iteration 
        DO j = 1, N-1
            y = j * dx
            DO i = 1, N-1
                
                x = i*dx
                f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
                u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f ) 
        
            END DO
        END DO

        ! compute l2 error
        l2err = SQRT(SUM((u-uexact)**2))
        
        ! compute residual
        CALL compute_residual(u,r)
        l2r = SQRT(SUM(r**2))

        ! increment iteration count
        k = k + 1

    END DO

    PRINT*,''
    PRINT*,'Iterations loop completed'
    PRINT*,'l2r = ',l2r  
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,'# of iterations = ',k
    IF(k .GE. maxiter) PRINT*,'Iterations failed to converge.'
    PRINT*,''

END SUBROUTINE solve_wjacobi



SUBROUTINE compute_residual(u,r)

    REAL, INTENT(IN) :: u(0:N,0:N)
    REAL, INTENT(OUT) :: r(0:N,0:N)
    INTEGER :: i, j
    REAL x, y, dx2, f

    dx2 = dx**2

    ! clear residual array
    r = 0.d0

    ! compute residual at every grid point
    DO j = 1, N-1
        y = j * dx
        DO i = 1, N-1
                
            x = i*dx
            f = -2.0*((1-6.0*x**2)*(y**2)*(1-y**2)+((1-6.0*y**2)*(x**2)*(1-x**2)))
            
            r(i,j) = u(i+1,j) + u(i-1,j) - 4.0 * u(i,j) + u(i,j+1) + u(i,j-1) - dx2*f  
        
        END DO
    END DO


END SUBROUTINE compute_residual
    

END PROGRAM poisson_2d
