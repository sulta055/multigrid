! Implementation of the two-grid iterative method for the 2D poissons equation
! Errors are computed relative to an exact solution

PROGRAM poisson_2d_2grid

USE MPI
IMPLICIT NONE

INTEGER, PARAMETER :: N = 256
REAL, PARAMETER :: tol = 1.e-2
REAL*8,  PARAMETER :: PI  = 4.d0*ATAN(1.d0)
LOGICAL, PARAMETER :: bypass2grid = .TRUE.
!INTEGER, PARAMETER :: smoother_type = 3 ! 1: JACOBI, 2: GS, 3: SOR, 4: WEIGHTED JACOBI
INTEGER :: i,j
REAL :: x, y, dx, dx2, t1, t2, t3
REAL, ALLOCATABLE :: u(:,:), f(:,:), uexact(:,:), r(:,:), uc(:,:), fc(:,:), u2(:,:)


ALLOCATE(u(0:N,0:N),f(0:N,0:N),uc(0:N/2,0:N/2),fc(0:N/2,0:N/2),uexact(0:N,0:N),r(0:N,0:N),u2(0:N,0:N))
u = 0.0
uc = 0.0
uexact = 0.0
u2 = 0.0

! grid resolution
dx = 1.d0/SNGL(N)
dx2 = dx**2

! fill exact solution array and source term array
DO j = 0, N
    y = j*dx
    DO i = 0, N
        x = i*dx
        uexact(i,j) = (x**2-x**4)*(y**4-y**2)        
        f(i,j) = - 2.0 * ((1.0-6.0*x**2)*(y**2)*(1.0-y**2)+((1.0-6.0*y**2)*(x**2)*(1.0-x**2)))
    END DO
END DO


! set initial guess
u = 0.d0

! solve poissons equation

t1 = MPI_WTIME()
CALL solve_2grid()
t2 = MPI_WTIME()
CALL solve_sor(N,300,u2,f,.FALSE.) 
t3 = MPI_WTIME()

! Save to file
OPEN(UNIT=10, FILE='output.dat', FORM = 'UNFORMATTED', ACCESS='STREAM')

DO j = 0, N
    y = j*dx
    DO i = 0, N
        x = i*dx
        WRITE(10) x,y,uexact(i,j),u(i,j),u2(i,j)
    END DO
END DO


CLOSE(UNIT=10)


DEALLOCATE(u,f,uc,fc,uexact,r,u2)


PRINT*,''
PRINT*,'One grid solve time (sec) = ',t3-t2
PRINT*,'Two grid solve time (sec) = ',t2-t1
PRINT*,''
PRINT*,'Done.'

CONTAINS


SUBROUTINE solve_2grid()

    INTEGER :: i, j, k, siters1, siters2, riters, cycles
    REAL :: l2err, l2r


    siters1 = 3  ! number of smoothing sweeps
    siters2 = 3  ! number of post-smoothing sweeps
    riters = 50 ! number of coarse-grid sweeps
    cycles = 2   ! number of two-grid cycles 

    
    DO k = 1, cycles
    
        !**********************************************************************************************************************************
        ! Step 1: Relaxation/ Smoothing Sweeps (i.e. we do a few iterations on the fine grid to smooth out the residuals on the fine grid)
        !********************************************************************************************************************************** 
        CALL solve_sor(N,siters1,u,f,.TRUE.) ! using SOR for smoothing 


        !*****************************************************************************************************************
        ! Step 2: Restriction/ Coarse-ification (we compute the fine grid residuals and transfer them to the coarse grid
        !         and compute an approximate solution of the error equation on the coarse grid)
        !*****************************************************************************************************************
        
        ! compute the fine-grid residual
        CALL compute_residual(N,u,r,f)
        
        ! transfer fine-grid residuals onto coarse grid via interpolation
        DO j = 0, N/2
            DO i = 0, N/2
                
                ! zeroth order interpolation
                !fc(i,j) = r(2*i,2*j)    

                ! linear interpolation (i.e. nine-point weighted average)
                fc(i,j) = (1.0/16.0) * (4.0 * r(2*i,2*j) + 2.0 * ( r(2*i-1,2*j)+r(2*i+1,2*j)+r(2*i,2*j+1)+r(2*i,2*j-1) ) + &
                                                                 ( r(2*i-1,2*j-1)+r(2*i-1,2*j+1)+r(2*i+1,2*j-1)+r(2*i+1,2*j+1) ))  
                                                                 
            END DO
        END DO
        
        ! relax on the coarse grid residual equation to obtain an approximate error, i.e. correction term
        uc = 0.0 ! initial error guess is zero
        CALL solve_sor(N/2,riters,uc,fc,.FALSE.)
       
        !***********************************************************************************************************************************
        ! Step 3: Prolongation/ Interpolation (we interpolate the error/correction from coarse back to the fine grid and add it to solution) 
        !***********************************************************************************************************************************

        ! linearly interpolated coarse-grid error/correction onto fine grid and add to the solution to get new improved initial guess 
        DO j = 2, N, 2
            DO i = 2, N, 2

                ! for even numbered points, no interpolation required
                u(i,j) = u(i,j) + uc(i/2,j/2) 

                ! for odd numbered points, we use linear interpolation
                u(i-1,j)   = u(i-1,j) + 0.5 * (uc(i/2 - 1,j/2) + uc(i/2,j/2)) 
                u(i-1,j-1) = u(i-1,j-1) + 0.25 * (uc(i/2 - 1,j/2) + uc(i/2,j/2) + uc(i/2,j/2 - 1) + uc(i/2-1,j/2-1))
                u(i,j-1)   = u(i,j-1) + 0.5 * (uc(i/2,j/2 - 1) + uc(i/2,j/2))          
                    
            END DO
        END DO

        !********************************************************************************************************************************************
        ! Step 4: Post Relaxation/ Smoothing Sweeps (i.e. we do some more iterations on the fine grid to refine the solution using new initial guess)
        !********************************************************************************************************************************************
        CALL solve_sor(N,siters2,u,f,.TRUE.) ! using SOR for smoothing  

        
    END DO    
         

    ! compute error l2 norm
    l2err = SQRT(SUM((u-uexact)**2))

    PRINT*,''
    PRINT*,'l2 error norm = ',l2err  
    PRINT*,''


END SUBROUTINE solve_2grid



! Successive Over-relaxation (SOR) iterations
SUBROUTINE solve_sor(nn,iters,u,f,relax)

    INTEGER, INTENT(IN) :: nn, iters
    REAL, INTENT(INOUT) :: u(0:nn,0:nn), f(0:nn,0:nn)
    LOGICAL, INTENT(IN) :: relax
    INTEGER :: i, j, k
    REAL :: x, y, dx, dx2, w, wopt, l2r, dl2r
    REAL, ALLOCATABLE :: rc(:,:)
    REAL, PARAMETER :: rtol = 1.d-5
    REAL, ALLOCATABLE :: buffer(:,:)
    
    
    !ALLOCATE(buffer(0:nn,0:nn))
    k = 0
    dl2r = 1.0
    l2r = rtol + dl2r
    dx = 1.0/SNGL(nn)
    dx2 = dx**2
    ! set relaxation parameter
    wopt = 2.0/(1.0+SIN(PI*dx)) ! SOR optimal value
    w = wopt             ! 1<w<=2
    
    
    ! for relaxation, we do a fixed number of iterations
    IF(relax) THEN
        
        ! relaxation/iterations loop
        DO k = 1, iters

            ! copy intial state into buffer
            !buffer = u

            ! compute next iteration 
            DO j = 1, nn-1
                DO i = 1, nn-1
                    
                    u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2*f(i,j)) 
                    !u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f(i,j)) 
            
                END DO
            END DO

        END DO

    ELSE
    
        ALLOCATE(rc(0:nn,0:nn))
    
        ! relaxation/iterations loop
        DO WHILE(k .LT. iters .AND. dl2r .GT. rtol)

            ! copy intial state into buffer
            !buffer = u

            ! compute next iteration 
            DO j = 1, nn-1
                DO i = 1, nn-1
                    
                    u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (u(i+1,j) + u(i-1,j) + u(i,j+1) + u(i,j-1) - dx2*f(i,j)) 
                    !u(i,j) = (1.0-w) * u(i,j) + w * 0.25 * (buffer(i+1,j) + buffer(i-1,j) + buffer(i,j+1) + buffer(i,j-1) - dx2*f(i,j)) 
            
                END DO
            END DO

            ! compute residual l2-norm
            CALL compute_residual(nn,u,rc,f)
            
            dl2r = l2r
            l2r = SQRT(SUM(rc**2))           
            dl2r = ABS(dl2r-l2r) 
            
            ! increment iteration count
            k = k + 1

        END DO
        
        PRINT*,'# of iterations = ',k
        PRINT*,'d12r = ',dl2r
        
        DEALLOCATE(rc)
    
    END IF


    !DEALLOCATE(buffer)

END SUBROUTINE solve_sor



SUBROUTINE compute_residual(nn,u,r,f)

    INTEGER, INTENT(IN) :: nn
    REAL, INTENT(IN) :: u(0:nn,0:nn), f(0:nn,0:nn)
    REAL, INTENT(OUT) :: r(0:nn,0:nn)
    INTEGER :: i, j
    REAL x, y, dx, idx2

    idx2 = SNGL(nn**2)
    
    ! clear residual array
    r = 0.d0

    ! compute residual at every grid point
    DO j = 1, nn-1
        DO i = 1, nn-1
            r(i,j) = f(i,j) - idx2*(u(i+1,j) + u(i-1,j) - 4.0 * u(i,j) + u(i,j+1) + u(i,j-1)) 
        END DO
    END DO


END SUBROUTINE compute_residual
    

END PROGRAM poisson_2d_2grid
